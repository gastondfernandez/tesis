-- MySQL dump 10.13  Distrib 5.5.21, for Win64 (x86)
--
-- Host: localhost    Database: sistemacontable
-- ------------------------------------------------------
-- Server version	5.5.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `sistemacontable`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `sistemacontable` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sistemacontable`;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNTPLAN_ID` int(11) DEFAULT NULL,
  `ITEM_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ACCOUNT_N49` (`ACCOUNTPLAN_ID`),
  KEY `ACCOUNT_N50` (`ITEM_ID`),
  CONSTRAINT `ACCOUNT_FK1` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `ACCOUNT_FK2` FOREIGN KEY (`ACCOUNTPLAN_ID`) REFERENCES `accountplan` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_account`
--

DROP TABLE IF EXISTS `account_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_account` (
  `ACCOUNT_ID` int(11) NOT NULL,
  `CHILD_ID` int(11) DEFAULT NULL,
  KEY `ACCOUNT_ACCOUNT_N49` (`CHILD_ID`),
  KEY `ACCOUNT_ACCOUNT_N50` (`ACCOUNT_ID`),
  CONSTRAINT `ACCOUNT_ACCOUNT_FK2` FOREIGN KEY (`CHILD_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `ACCOUNT_ACCOUNT_FK1` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_account`
--

LOCK TABLES `account_account` WRITE;
/*!40000 ALTER TABLE `account_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountplan`
--

DROP TABLE IF EXISTS `accountplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountplan` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountplan`
--

LOCK TABLES `accountplan` WRITE;
/*!40000 ALTER TABLE `accountplan` DISABLE KEYS */;
INSERT INTO `accountplan` VALUES (1);
/*!40000 ALTER TABLE `accountplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'ACTIVO'),(2,'PASIVO'),(3,'PATRIMONIO NETO'),(4,'RESULTADOS');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNTPLAN_ID` int(11) DEFAULT NULL,
  `ADDRESS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CONDFISCAL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUIT` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `IVA` float NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PHONE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TIPODOC_ID` int(11) DEFAULT NULL,
  `ZIPCODE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CUIT` (`CUIT`),
  KEY `CLIENT_N49` (`ACCOUNTPLAN_ID`),
  KEY `CLIENT_N50` (`TIPODOC_ID`),
  CONSTRAINT `CLIENT_FK1` FOREIGN KEY (`ACCOUNTPLAN_ID`) REFERENCES `accountplan` (`ID`),
  CONSTRAINT `CLIENT_FK2` FOREIGN KEY (`TIPODOC_ID`) REFERENCES `idtype` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (2,1,NULL,NULL,NULL,NULL,10,'ELPIBE S.A.',NULL,NULL,NULL);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise`
--

DROP TABLE IF EXISTS `enterprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CONDFISCAL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUIT` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `IVA` float NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PHONE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TIPODOC_ID` int(11) DEFAULT NULL,
  `ZIPCODE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CUIT` (`CUIT`),
  KEY `ENTERPRISE_N49` (`TIPODOC_ID`),
  CONSTRAINT `ENTERPRISE_FK1` FOREIGN KEY (`TIPODOC_ID`) REFERENCES `idtype` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise`
--

LOCK TABLES `enterprise` WRITE;
/*!40000 ALTER TABLE `enterprise` DISABLE KEYS */;
INSERT INTO `enterprise` VALUES (1,NULL,NULL,NULL,NULL,10,'SHELL',NULL,NULL,NULL);
/*!40000 ALTER TABLE `enterprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise_account`
--

DROP TABLE IF EXISTS `enterprise_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_account` (
  `ENTERPRISE_ID` int(11) NOT NULL,
  `ACCOUNTS_ID` int(11) DEFAULT NULL,
  KEY `ENTERPRISE_ACCOUNT_N50` (`ENTERPRISE_ID`),
  KEY `ENTERPRISE_ACCOUNT_N49` (`ACCOUNTS_ID`),
  CONSTRAINT `ENTERPRISE_ACCOUNT_FK2` FOREIGN KEY (`ACCOUNTS_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `ENTERPRISE_ACCOUNT_FK1` FOREIGN KEY (`ENTERPRISE_ID`) REFERENCES `enterprise` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise_account`
--

LOCK TABLES `enterprise_account` WRITE;
/*!40000 ALTER TABLE `enterprise_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `enterprise_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idtype`
--

DROP TABLE IF EXISTS `idtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `idtype` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idtype`
--

LOCK TABLES `idtype` WRITE;
/*!40000 ALTER TABLE `idtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `idtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUBCAT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`),
  KEY `ITEM_N49` (`SUBCAT_ID`),
  CONSTRAINT `ITEM_FK1` FOREIGN KEY (`SUBCAT_ID`) REFERENCES `subcategory` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'CAJA Y BANCOS',1),(2,'CREDITOS POR VENTAS',1),(3,'OTROS CREDITOS',1),(4,'BIENES DE CAMBIO',1),(5,'BIENES DE USO',2),(6,'PARTIDAS EN SUSPENSO',3),(7,'DEUDAS COMERCIALES',4),(8,'DEUDAS SOCIALES',4),(9,'DEUDAS FISCALES',4),(10,'DEUDAS BANCARIAS',4),(11,'CUENTAS PARTICULARES SOCIOS',4),(12,'OTRAS DEUDAS',4),(13,'CAPITAL',6),(14,'RESERVAS',6),(15,'RESULTADOS',6),(16,'INGRESOS POR VENTAS Y SERVICIOS',7),(17,'OTROS INGRESOS',7),(18,'COSTO DE VENTAS',8),(19,'GASTOS DE COMERCIALIZACION',8),(20,'GASTOS DE ADMINISTRACION',8),(21,'GASTOS FINANCIEROS',8),(22,'OTROS EGRESOS',8);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ENABLE` bit(1) NOT NULL,
  `PATH` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VERSION` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAT_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`),
  KEY `SUBCATEGORY_N49` (`CAT_ID`),
  CONSTRAINT `SUBCATEGORY_FK1` FOREIGN KEY (`CAT_ID`) REFERENCES `category` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES (1,1,'ACTIVO CORRIENTE'),(2,1,'ACTIVO NO CORRIENTE'),(3,1,'CUENTAS TRANSITORIAS'),(4,2,'PASIVO CORRIENTE'),(5,2,'PASIVO NO CORRIENTE'),(6,3,'CAPITAL, RESERVAS Y RESULTADOS'),(7,4,'INGRESOS'),(8,4,'GASTOS');
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTDATE` date DEFAULT NULL,
  `EMISSIONDATE` date DEFAULT NULL,
  `IMPFDATE` date DEFAULT NULL,
  `NUMBER` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRICE` float NOT NULL,
  `RETENCIONS` float NOT NULL,
  `TYPE_ID` int(11) DEFAULT NULL,
  `UNGRAVED` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `TICKET_N49` (`TYPE_ID`),
  CONSTRAINT `TICKET_FK1` FOREIGN KEY (`TYPE_ID`) REFERENCES `tticket` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_account`
--

DROP TABLE IF EXISTS `ticket_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_account` (
  `TICKETS_ID` int(11) NOT NULL,
  `ACCOUNTS_ID` int(11) NOT NULL,
  KEY `TICKET_ACCOUNT_N49` (`TICKETS_ID`),
  KEY `TICKET_ACCOUNT_N50` (`ACCOUNTS_ID`),
  CONSTRAINT `TICKET_ACCOUNT_FK1` FOREIGN KEY (`TICKETS_ID`) REFERENCES `ticket` (`ID`),
  CONSTRAINT `TICKET_ACCOUNT_FK2` FOREIGN KEY (`ACCOUNTS_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_account`
--

LOCK TABLES `ticket_account` WRITE;
/*!40000 ALTER TABLE `ticket_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tticket`
--

DROP TABLE IF EXISTS `tticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tticket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tticket`
--

LOCK TABLES `tticket` WRITE;
/*!40000 ALTER TABLE `tticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PASS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ROLE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-14 19:24:04

