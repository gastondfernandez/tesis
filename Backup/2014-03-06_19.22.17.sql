-- MySQL dump 10.13  Distrib 5.5.21, for Win64 (x86)
--
-- Host: localhost    Database: sistemacontable
-- ------------------------------------------------------
-- Server version	5.5.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `sistemacontable`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `sistemacontable` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sistemacontable`;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNTPLAN_ID` int(11) DEFAULT NULL,
  `ITEM_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ACCOUNT_N49` (`ITEM_ID`),
  KEY `ACCOUNT_N50` (`ACCOUNTPLAN_ID`),
  CONSTRAINT `ACCOUNT_FK1` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `ACCOUNT_FK2` FOREIGN KEY (`ACCOUNTPLAN_ID`) REFERENCES `accountplan` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,1,1,'CAJA'),(2,1,1,'BANCO'),(3,1,1,'DEPOSITOS DE CAUCION'),(4,1,1,'CAJA DE AHORRO'),(5,1,2,'CLIENTES'),(6,1,2,'TARJETAS A COBRAR'),(7,1,3,'IVA CREDITO FISCAL'),(8,1,3,'RETENCIONES INGRESOS BRUTOS'),(9,1,3,'RETENCIONES IVA'),(10,1,3,'RETENCIONES GANANCIAS'),(11,1,3,'OTRAS CUENTAS POR COBRAR'),(12,1,4,'MATERIAS PRIMAS'),(13,1,4,'PRODUCTOS EN PROCESO'),(14,1,4,'PRODUCTOS TERMINADOS'),(15,1,4,'MERCADERIAS DE REVENTA'),(16,1,5,'MUEBLES Y UTILES'),(17,1,5,'AMORT. ACUM. MUEBLES Y UTILES'),(18,1,5,'RODADOS'),(19,1,5,'AMORT. ACOM. RODADOS'),(20,1,5,'INSTALACIONES'),(21,1,5,'AMORT. ACOM. INSTALACIONES'),(22,1,5,'EDIFICIOS'),(23,1,5,'AMORT. ACOM. EDIFICIOS'),(24,1,7,'PROVEEDORES'),(25,1,8,'SUELDOS A PAGAR'),(26,1,8,'SUSS A PAGAR'),(27,1,8,'A.E.C. A PAGAR'),(28,1,8,'FAECYS A PAGAR'),(29,1,8,'LA ESTRELLA A PAGAR'),(30,1,8,'OTRAS LEYES SOC. A PAGAR'),(31,1,10,'PRESTAMOS Y ACUERDOS BANCARIOS'),(32,1,12,'I.V.A DEBITO FISCAL'),(33,1,12,'IVA RESP. NO INSCRIPTO'),(34,1,12,'ALQUILERES A PAGAR'),(35,1,12,'SEGUROS A PAGAR'),(36,1,13,'CAPITAL SOCIAL'),(37,1,13,'AJUSTE AL CAPITAL'),(38,1,13,'APORTE IRREVOCABLE A CTA. FUT. SUSC.'),(39,1,14,'RESERVA LEGAL'),(40,1,15,'RESULTADO DEL EJERCICIO'),(41,1,15,'RESULTADOS NO ASIGNADOS'),(42,1,15,'AJUSTE EJERCICIOS ANTERIORES'),(43,1,16,'VENTAS POR MAYOR'),(44,1,16,'VENTAS POR MENOR'),(45,1,16,'VENTAS AL EXTERIOR'),(46,1,17,'DESCUENTOS OBTENIDOS'),(47,1,17,'DONIFICACIONES OBTENIDAS'),(48,1,17,'INTERESES GANADOS'),(49,1,17,'GASTOS RECUPERADOS'),(50,1,17,'OTROS INGRESOS'),(51,1,18,'COSTOS DE MERCADERIAS VENDIDAS'),(52,1,18,'INSUMOS MATERIAS PRIMAS'),(53,1,18,'AMORTIZACIONES'),(54,1,18,'SUELDOS FABRICA'),(55,1,18,'CARGAS SOCIALES FABRICA'),(56,1,18,'GASTOS VARIOS FABRICA'),(57,1,19,'SUELDOS COMERCIALIZACION'),(58,1,19,'CARGAS SOCIALES COMERCIALIZACION'),(59,1,19,'GASTOS DE EMBALAJE Y FLETES'),(60,1,19,'MOVILIDAD'),(61,1,19,'IMPUESTOS Y TASAS'),(62,1,19,'ALQUILERES'),(63,1,19,'PUBLICIDAD Y IMPRENTA'),(64,1,19,'BONIFICACIONES CONCEDIDAS'),(65,1,19,'INGRESOS BRUTOS'),(66,1,19,'GASTPS VARIOS DE COMERCIALIZACION'),(67,1,20,'HONORARIOS'),(68,1,20,'SUELDOS ADMINISTRACION'),(69,1,20,'CARGAS SOCIALES ADMINISTRACION'),(70,1,20,'PAPELERIA Y UTILES DE OFICINA'),(71,1,20,'GASTOS VARIOS DE ADMINISTRACION'),(72,1,22,'SEGUROS');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_account`
--

DROP TABLE IF EXISTS `account_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_account` (
  `ACCOUNT_ID` int(11) NOT NULL,
  `CHILD_ID` int(11) DEFAULT NULL,
  KEY `ACCOUNT_ACCOUNT_N49` (`CHILD_ID`),
  KEY `ACCOUNT_ACCOUNT_N50` (`ACCOUNT_ID`),
  CONSTRAINT `ACCOUNT_ACCOUNT_FK2` FOREIGN KEY (`CHILD_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `ACCOUNT_ACCOUNT_FK1` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_account`
--

LOCK TABLES `account_account` WRITE;
/*!40000 ALTER TABLE `account_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountplan`
--

DROP TABLE IF EXISTS `accountplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountplan` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountplan`
--

LOCK TABLES `accountplan` WRITE;
/*!40000 ALTER TABLE `accountplan` DISABLE KEYS */;
INSERT INTO `accountplan` VALUES (1);
/*!40000 ALTER TABLE `accountplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'ACTIVO'),(2,'PASIVO'),(3,'PATRIMONIO NETO'),(4,'RESULTADOS');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNTPLAN_ID` int(11) DEFAULT NULL,
  `ADDRESS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CONDFISCAL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUIT` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `IVA` float NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PHONE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TIPODOC` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ZIPCODE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CUIT` (`CUIT`),
  KEY `CLIENT_N49` (`ACCOUNTPLAN_ID`),
  CONSTRAINT `CLIENT_FK1` FOREIGN KEY (`ACCOUNTPLAN_ID`) REFERENCES `accountplan` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,1,'','IVA_RESPONSABLE_INSCRIPTO','12345','',21,'ELPIBE SA','','CUIT',NULL);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise`
--

DROP TABLE IF EXISTS `enterprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CONDFISCAL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUIT` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `IVA` float NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PHONE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TIPODOC` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ZIPCODE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CUIT` (`CUIT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise`
--

LOCK TABLES `enterprise` WRITE;
/*!40000 ALTER TABLE `enterprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `enterprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise_account`
--

DROP TABLE IF EXISTS `enterprise_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_account` (
  `ENTERPRISE_ID` int(11) NOT NULL,
  `ACCOUNTS_ID` int(11) DEFAULT NULL,
  KEY `ENTERPRISE_ACCOUNT_N50` (`ENTERPRISE_ID`),
  KEY `ENTERPRISE_ACCOUNT_N49` (`ACCOUNTS_ID`),
  CONSTRAINT `ENTERPRISE_ACCOUNT_FK2` FOREIGN KEY (`ACCOUNTS_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `ENTERPRISE_ACCOUNT_FK1` FOREIGN KEY (`ENTERPRISE_ID`) REFERENCES `enterprise` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise_account`
--

LOCK TABLES `enterprise_account` WRITE;
/*!40000 ALTER TABLE `enterprise_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `enterprise_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUBCAT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`),
  KEY `ITEM_N49` (`SUBCAT_ID`),
  CONSTRAINT `ITEM_FK1` FOREIGN KEY (`SUBCAT_ID`) REFERENCES `subcategory` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'CAJA Y BANCOS',1),(2,'CREDITOS POR VENTAS',1),(3,'OTROS CREDITOS',1),(4,'BIENES DE CAMBIO',1),(5,'BIENES DE USO',2),(6,'PARTIDAS EN SUSPENSO',3),(7,'DEUDAS COMERCIALES',4),(8,'DEUDAS SOCIALES',4),(9,'DEUDAS FISCALES',4),(10,'DEUDAS BANCARIAS',4),(11,'CUENTAS PARTICULARES SOCIOS',4),(12,'OTRAS DEUDAS',4),(13,'CAPITAL',6),(14,'RESERVAS',6),(15,'RESULTADOS',6),(16,'INGRESOS POR VENTAS Y SERVICIOS',7),(17,'OTROS INGRESOS',7),(18,'COSTO DE VENTAS',8),(19,'GASTOS DE COMERCIALIZACION',8),(20,'GASTOS DE ADMINISTRACION',8),(21,'GASTOS FINANCIEROS',8),(22,'OTROS EGRESOS',8);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ENABLE` bit(1) NOT NULL,
  `PATH` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VERSION` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAT_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`),
  KEY `SUBCATEGORY_N49` (`CAT_ID`),
  CONSTRAINT `SUBCATEGORY_FK1` FOREIGN KEY (`CAT_ID`) REFERENCES `category` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES (1,1,'ACTIVO CORRIENTE'),(2,1,'ACTIVO NO CORRIENTE'),(3,1,'CUENTAS TRANSITORIAS'),(4,2,'PASIVO CORRIENTE'),(5,2,'PASIVO NO CORRIENTE'),(6,3,'CAPITAL, RESERVAS Y RESULTADOS'),(7,4,'INGRESOS'),(8,4,'GASTOS');
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTDATE` date DEFAULT NULL,
  `EMISSIONDATE` date DEFAULT NULL,
  `ENTERPRISE_ID` int(11) DEFAULT NULL,
  `IMPFDATE` date DEFAULT NULL,
  `NUMBER` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRICE` decimal(19,0) DEFAULT NULL,
  `RETENCIONS` decimal(19,0) DEFAULT NULL,
  `TOP` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_ID` int(11) DEFAULT NULL,
  `UNGRAVED` decimal(19,0) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TICKET_N49` (`ENTERPRISE_ID`),
  KEY `TICKET_N50` (`TYPE_ID`),
  CONSTRAINT `TICKET_FK2` FOREIGN KEY (`ENTERPRISE_ID`) REFERENCES `enterprise` (`ID`),
  CONSTRAINT `TICKET_FK1` FOREIGN KEY (`TYPE_ID`) REFERENCES `tticket` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_account`
--

DROP TABLE IF EXISTS `ticket_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_ID` int(11) DEFAULT NULL,
  `PRICE` decimal(19,0) DEFAULT NULL,
  `TICKET_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TICKET_ACCOUNT_N49` (`TICKET_ID`),
  KEY `TICKET_ACCOUNT_N50` (`ACCOUNT_ID`),
  CONSTRAINT `TICKET_ACCOUNT_FK1` FOREIGN KEY (`TICKET_ID`) REFERENCES `ticket` (`ID`),
  CONSTRAINT `TICKET_ACCOUNT_FK2` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_account`
--

LOCK TABLES `ticket_account` WRITE;
/*!40000 ALTER TABLE `ticket_account` DISABLE KEYS */;
INSERT INTO `ticket_account` VALUES (1,1,3,1);
/*!40000 ALTER TABLE `ticket_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tticket`
--

DROP TABLE IF EXISTS `tticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tticket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tticket`
--

LOCK TABLES `tticket` WRITE;
/*!40000 ALTER TABLE `tticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PASS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ROLE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('Usuario','Clave','user'),('elPibe','a',NULL),('laPiba','b','user'),('root','root',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-06 19:22:17

