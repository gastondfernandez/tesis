/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class UserControllerTest {

    private String nameOk = "junit", nameFail = "junit2";
    private String pass = "junit";
    private String roleAdmin = null, roleUser = "user";

    /**
     *
     */
    public UserControllerTest() {
    }

    /**
     *
     */
    @After
    public void deleteTempUser() {
        UserController.delete(nameOk);
    }

    /**
     * Test of insert method, of class UserController.
     */
    @Test
    public void testInsertOk() {
        boolean expResult = true;
        boolean result = UserController.insert(nameOk, pass, roleAdmin);
        assertEquals(expResult, result);
    }

    /**
     * Test of insert method, of class UserController.
     */
    @Test
    public void testFailInsert() {
        UserController.insert(nameOk, pass, roleAdmin);
        boolean expResult = false;
        boolean result = UserController.insert(nameOk, pass, roleAdmin);
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class UserController.
     */
    @Test
    public void testDeleteOK() {
        boolean insert = UserController.insert(nameFail, pass, roleAdmin);
        boolean delete = UserController.delete(nameFail);
        assertEquals(insert, delete);
    }

    /**
     * Test of delete method, of class UserController.
     */
    @Test
    public void testDeleteFail() {
        boolean delete = UserController.delete(nameFail);
        assertFalse(delete);
    }

    /**
     * Test of changePass method, of class UserController.
     */
    @Test
    public void testChangePassOk() {
        boolean expResult = true;
        UserController.insert(nameOk, pass, roleAdmin);
        boolean result = UserController.changePass(nameOk, pass);
        assertEquals(expResult, result);
    }

    /**
     * Test of changePass method, of class UserController.
     */
    @Test
    public void testChangePassFail() {
        UserController.insert(nameOk, pass, roleAdmin);
        boolean expResult = false;
        boolean result = UserController.changePass(nameFail, pass);
        assertEquals(expResult, result);
    }

    /**
     * Test of changeRole method, of class UserController.
     */
    @Test
    public void testChangeRoleOk() {
        UserController.insert(nameOk, pass, roleAdmin);
        boolean expResult = true;
        boolean result = UserController.changeRole(nameOk, roleUser);
        assertEquals(expResult, result);
    }

    /**
     * Test of changeRole method, of class UserController.
     */
    @Test
    public void testChangeRoleFail() {
        UserController.insert(nameOk, pass, roleAdmin);
        boolean expResult = false;
        boolean result = UserController.changeRole(nameFail, roleUser);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAllUsers method, of class UserController.
     */
    @Test
    public void testGetAllUsers() {
        UserController.insert(nameOk, pass, roleAdmin);
        int expResult = 0;
        int result = UserController.getAllUsers().size();
        assertNotSame(expResult, result);
    }
}