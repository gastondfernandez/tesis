/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 *
 * @author lprone
 */
public class Backup {

    private String user, pass, host = "localhost";

    /**
     * Default coonstructor
     *
     * @param user
     * @param pass
     */
    public Backup(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    /**
     * Make SQLFile with dataBase data
     *
     * @param dbName
     * @param fileName
     * @return
     */
    public boolean MakeBackupSQL(String dbName, String fileName) {
        int BUFFER = 10485760;
        StringBuffer temp;
        FileWriter fichero;
        PrintWriter pw;
        try {
            Process run = Runtime.getRuntime().exec("mysqldump --host=" + host + " --user=" + user + " --password=" + pass + " --databases " + dbName);
            InputStream in = run.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            temp = new StringBuffer();
            int count;
            char[] cbuf = new char[BUFFER];
            while ((count = br.read(cbuf, 0, BUFFER)) != -1) {
                temp.append(cbuf, 0, count);
            }
            br.close();
            in.close();
            fichero = new FileWriter(fileName);
            pw = new PrintWriter(fichero);
            pw.println(temp.toString());
            fichero.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Restore database from SQLFile
     *
     * @param fileName
     * @return
     */
    public boolean RestoreBackupSQL(String fileName) {
        try {
            String[] executeCmd = new String[]{"mysql", "--host=" + host, "--user=" + user, "--password=" + pass, "-e", "source Backup/" + fileName};
            Runtime.getRuntime().exec(executeCmd);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }
}
