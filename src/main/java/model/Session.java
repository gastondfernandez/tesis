/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Save data to loggued user
 *
 * @author lprone
 */
public class Session {

    private static User user;

    /**
     *
     * @param u
     * @return
     */
    public static boolean setSession(User u) {
        user = u;
        return true;
    }

    /**
     *
     * @return
     */
    public static boolean closeSession() {
        user = null;
        return true;
    }

    /**
     *
     * @return
     */
    public static User getSession() {
        return user;
    }
}
