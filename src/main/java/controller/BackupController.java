/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import model.Backup;

/**
 * Make and restore backup files
 *
 * @author lprone
 */
public class BackupController {

    private static String /**
             *
             */
            user = "",
            /**
             *
             */
            pass = "",
            /**
             *
             */
            dbName = "sistemacontable";

    /**
     * Get connection data from properties file
     */
    private static void init() {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("./config.properties"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        user = props.getProperty("javax.persistence.jdbc.user");
        pass = props.getProperty("javax.persistence.jdbc.password");
    }

    /**
     *
     */
    public static void MakeBackup() {
        init();
        if (new Backup(user, pass).MakeBackupSQL(dbName, System.getProperty("user.dir") + "/Backup/" + new SimpleDateFormat("yyyy-MM-dd_H.mm.ss").format(new Date()) + ".sql")) {
            System.out.println("OK");
        } else {
            System.out.println("FALLO");
        }
    }

    /**
     *
     * @param fileName
     * @return
     */
    public static boolean RestoreBackup(String fileName) {
        init();
        if (new Backup(user, pass).RestoreBackupSQL(fileName)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        MakeBackup();
    }
}
