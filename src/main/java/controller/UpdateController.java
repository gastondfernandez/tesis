/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.JProgressBar;
import model.Module;

/**
 *
 * @author gdf
 */
public class UpdateController {

    private static JProgressBar aux;

    /**
     *
     * @param id
     * @return true when a module has update
     */
    private static boolean isUpgradeable(int id) {
        String pathDb = ModuleController.getModule(id).getPath();

        File fichero = new File(pathDb);
        long ms = fichero.lastModified();
        Date d = new Date(ms);
        Calendar c = new GregorianCalendar();
        c.setTime(d);
        String dia = Integer.toString(c.get(Calendar.DATE));
        String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
        String annio = Integer.toString(c.get(Calendar.YEAR));

        String dateFile = annio + mes + dia;
        String dateInternet = ModuleController.getModInternet(id).getSecond();

        return (dateInternet.compareTo(dateFile)) > 0 ? true : false;
    }

    /**
     * Module version from internet
     *
     * @param id
     * @return
     */
    private static ArrayList<String> getModInternet(int id) {
        ArrayList<ArrayList<String>> lista = ReadXMLController.listVer();
        ArrayList<String> resultado = new ArrayList<String>();
        int version;
        for (int i = 0; i < lista.size(); i++) {
            version = Integer.parseInt(lista.get(i).get(0));
            if (version == id) {
                resultado = lista.get(i);
            }
        }
        return resultado;
    }

    /**
     * Download and update module
     *
     * @param module
     * @param bar
     */
    public static void updateModule(Module module, JProgressBar bar) {
        //examino nueva version
        ArrayList<String> resultado = getModInternet(module.getId());
        aux = bar;
        //obtengo la direccion del nuevo jar
        String dirInternet = resultado.get(3);
        String packageName = module.getDescription().replace(" ", "_") + "_PACK.jar";
        String fullPackagePath = System.getProperty("user.dir") + "/Modulos/" + packageName;

        System.out.println("-------updateModule----------");
        System.out.println("dirInternet: " + dirInternet);
        System.out.println("packageName" + packageName);
        System.out.println("fullPackagePath" + fullPackagePath);

        if (isUpgradeable(module.getId())) {
            try {
                if (downloadJar(module.getId(), dirInternet, fullPackagePath)) {
                    unpackJar(packageName);
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * get jar if is necessary
     *
     * @param id
     * @return
     */
    private static boolean downloadJar(int id, String web, String path) {
        System.out.println("-------downloadJAR-------");
        System.out.println("id: " + id);
        System.out.println("web: " + web);
        System.out.println("path: " + path);
        if (isUpgradeable(id)) {
            try {
                getFileFromWeb(web, path);
                return true;

            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    /**
     * Unpack jar file
     *
     * @param jarFile
     * @return
     */
    private static boolean unpackJar(String jarFile) {
        try {
            System.out.println("---------unpack---------");
            System.out.println("cmd /c start unpack.bat " + jarFile);
            if (System.getProperty("os.name").startsWith("Windows")) {
                Runtime.getRuntime().exec("cmd /c start unpack.bat " + jarFile);
            } else {
                Runtime.getRuntime().exec("unpack.sh " + jarFile);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Download file from update server
     *
     * @param web
     * @param path
     */
    private static void getFileFromWeb(String web, String path) {
        System.out.println("---------getFileFromWeb-----------");
        System.out.println("web: " + web);
        System.out.println("path: " + path);

        try {
            URL url = new URL(web);
            URLConnection urlCon = url.openConnection();
            int total = urlCon.getContentLength();
            int downloaded = 0;
            aux.setMaximum(total);
            InputStream is = urlCon.getInputStream();
            FileOutputStream fos = new FileOutputStream(path.toUpperCase());
            byte[] array = new byte[1000];
            int leido = is.read(array);
            downloaded += leido;
            while (leido > 0) {
                fos.write(array, 0, leido);
                leido = is.read(array);
                downloaded += leido;
                aux.setValue(downloaded);
            }
            is.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return list with modules to update
     */
    public static ArrayList<Module> modulesToUpdate() {
        ArrayList<Module> resultado = new ArrayList<Module>();
        List<Module> lista = ModuleController.getAllModules();
        for (Module module : lista) {
            module.getPath();
            if (isUpgradeable(module.getId())) {
                resultado.add(module);
            }
        }
        return resultado;
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
//        ModuleController.insert(2, "SERVER", true, 1.0f, "C:\\NetBeansProjects\\tesis Sistema\\Modulos\\server.jar");
        System.out.println(isUpgradeable(2));
        updateModule(ModuleController.getModule(2), null);
//        unpackJar("server.jar");
    }
}
