/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author gdf
 */
public class ReadXMLController {

    private static final String urlServer = "http://lprone.com.ar/tesis/versiones.xml";

    /**
     * explore xml tags
     *
     * @param tag
     * @param elemento
     * @return
     */
    private static String getTagValue(String tag, Element elemento) {
        NodeList lista = elemento.getElementsByTagName(tag).item(0).getChildNodes();
        Node valor = (Node) lista.item(0);
        return valor.getNodeValue();
    }

    /**
     *
     * @return version of modules from internet
     */
    public static ArrayList<ArrayList<String>> listVer() {
        ArrayList<ArrayList<String>> resultado = new ArrayList<ArrayList<String>>();
        try {
            resultado = getListVer();
        } catch (Exception ex) {
            System.out.println("Error obtener versiones 1");
        }
        return resultado;
    }

    /**
     *
     * @return modules's versions
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private static ArrayList<ArrayList<String>> getListVer() throws ParserConfigurationException, SAXException, IOException {
        URL url = new URL(urlServer);
        URLConnection yc = url.openConnection();
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(yc.getInputStream());
        doc.getDocumentElement().normalize();

        NodeList modulesList = doc.getElementsByTagName("modulo");
        Element elemento;
        ArrayList<ArrayList<String>> ret = new ArrayList();
        ArrayList<String> modulo;
        for (int i = 0; i < modulesList.getLength(); i++) {
            Node modulonode = (Node) modulesList.item(i);
            if (modulonode.getNodeType() == Node.ELEMENT_NODE) {
                modulo = new ArrayList();
                elemento = (Element) modulonode;
                modulo.add(getTagValue("id", elemento));
                modulo.add(getTagValue("nombre", elemento));
                modulo.add(getTagValue("date", elemento));
                modulo.add(getTagValue("jar", elemento));
                ret.add(modulo);
            }
        }
        return ret;
    }
}
