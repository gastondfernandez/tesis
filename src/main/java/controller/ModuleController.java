/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.ArrayList;
import java.util.List;
import model.Module;
import model.Pair;

/**
 *
 * @author lprone
 */
public class ModuleController {

    /**
     * create new module in database
     *
     * @param id
     * @param description
     * @param enable
     * @param version
     * @param path
     * @return
     */
    public static boolean insert(int id, String description, boolean enable,
            Float version, String path) {
        Module m = new Module(id, description, enable, version, path);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(m);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     * delete module from database
     *
     * @param id
     * @return
     */
    public static boolean delete(String id) {
        Module m = (Module) JPA.findByID(Module.class, id);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.delete(m);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @param enable
     * @return
     */
    public static boolean setEnable(String id, boolean enable) {
        Module m = (Module) JPA.findByID(Module.class, id);
        m.setEnable(enable);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.update(m);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @param version
     * @return
     */
    public static boolean setVersion(int id, float version) {
        Module m = (Module) JPA.findByID(Module.class, id);
        m.setVersion(version);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.update(m);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     * return module from id
     *
     * @param id
     * @return
     */
    public static Module getModule(int id) {
        return (Module) JPA.findByID(Module.class, id);
    }

    /**
     *
     * @return all modules list
     */
    public static List getAllModules() {
        return JPA.getAll(Module.class);
    }

    /**
     * Get information about modules update from internet
     *
     * @param id
     * @return
     */
    public static Pair<Integer, String> getModInternet(int id) {
        ArrayList<ArrayList<String>> lista = ReadXMLController.listVer();
        Pair<Integer, String> resultado = new Pair(null, null);
        int idI;
        for (int i = 0; i < lista.size(); i++) {
            idI = Integer.parseInt(lista.get(i).get(0));
            if (idI == id) {
                resultado.setFirst(Integer.parseInt(lista.get(i).get(0)));
                resultado.setSecond(lista.get(i).get(2));
            }
        }
        return resultado;
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        insert(2, "WEB SERVER", true, 0F, "Modulos/Web Server/server-1.0.jar");
    }
}