/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author gdf
 */
public class JPA {

    private static EntityManager em = DbConnection.getInstance();
    private static EntityTransaction tx = em.getTransaction();

    /**
     * Start new transaction
     */
    public static void beginTransaction() {
        tx.begin();
    }

    /**
     * Close active transaction
     */
    public static void commitTransaction() {
        tx.commit();
    }

    /**
     * Rollback active transaction
     */
    public static void rollbackTransaction() {
        if (tx.isActive()) {
            tx.rollback();
        }
    }

    /**
     * Create new object
     *
     * @param objeto
     * @return
     */
    public static boolean create(Object objeto) {
        try {
            em.persist(objeto);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Delete object
     *
     * @param obj
     * @return
     */
    public static boolean delete(Object obj) {
        try {
            em.remove(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Search object in database
     *
     * @param type
     * @param id
     * @return
     */
    public static Object findByID(Class type, Object id) {
        Object obj = null;
        try {
            obj = em.find(type, id);
        } catch (Exception e) {
            System.out.println("Eror al buscar");
        }
        return obj;
    }

    /**
     * Update object values in database
     *
     * @param obj
     * @return
     */
    public static boolean update(Object obj) {
        try {
            em.merge(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @param t
     * @return All values of a table
     */
    public static List<Object> getAll(Class t) {
        try {
            return em.createQuery("SELECT e FROM " + t.getName() + " e").getResultList();
        } catch (Exception e) {
            return null;
        }
    }
}
