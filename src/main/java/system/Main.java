package system;

import controller.UserController;
import db.JPA;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.User;

/**
 *
 * @author lprone
 */
public class Main {

    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        if (UserController.getAllUsers().isEmpty()) {
            new MainFrame().setVisible(true);
        } else {
            new Login().setVisible(true);
        }
    }
}
